import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public logindata={
    email:'',
    password:'',
  }
  public emailError = false;
  public passwordError = false;

  constructor(private route: Router) { }

  ngOnInit() {
  }

  loginMember(){

    console.log(this.logindata);

    this.emailError = !this.logindata[ 'email'] ? true: false;
    this.passwordError = !this.logindata[ 'password'] ? true: false;
    if(!this.emailError && !this.passwordError){
      this.route.navigateByUrl('/home');
    }
  }
}
