import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { registerLocaleData } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ShopComponent } from './shop/shop.component';


const routes: Routes = [
  {path: "", pathMatch: "full", redirectTo: "home"},
  { path: "home", component: HomeComponent },
  {path:"login", component: LoginComponent},
  { path: "register", component: RegisterComponent},
  { path: "shop", component: ShopComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
