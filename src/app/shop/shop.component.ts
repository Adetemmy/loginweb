import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/todo';
 
@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
   todoValue: String;
   list:  [];
  name: string;

  constructor(public todo: Todo) { }

  ngOnInit() {
     this.list = [];
     this.todoValue = "";
     this.name = " ";
  }

  addItem( Todo){
     if (this.todoValue !=="") {
        const newItem: Todo = {
         id: Date.now(),
          value: this.name,
          isDone: false,
        };
        this.list.push(newItem); 
     }
     this.name = "";
   }

   deleteItem(id: number){
     this.list = this.list.filter(item => item. !== id);
   }

}
