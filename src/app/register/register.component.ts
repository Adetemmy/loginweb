import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerData={
     first:'',
     last:'',
     email:'',
     password:'',
     phone:'',
  };

  public firstError = false;
  public lastError = false;
  public  phoneError = false;
  public emailError = false;
  public passwordError = false;
  constructor( private route: Router) { }

  ngOnInit() {
  }

  RegisterMember(){
    
    console.log(this.registerData);

   this.firstError = !this.registerData['first'] ? true: false;
   this.lastError = !this.registerData[ 'last'] ? true: false;
   this.phoneError = !this.registerData[ 'phone'] ? true: false;
   this.emailError = !this.registerData[ 'email'] ? true: false;
   this.passwordError = !this.registerData[ 'password'] ? true: false;
   if(!this.firstError && !this.passwordError && !this.lastError && !this.emailError && !this.phoneError){
    this.route.navigateByUrl('/login');
  }
  }
}
 